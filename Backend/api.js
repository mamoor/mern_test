const express=require('express');
const cors=require('cors');//import cors
const sha1=require('sha1');//encrypt data into 40 digit value
const bodyParser=require('body-parser');//parse body data
//multer file upload 
const multer=require('multer');
const nodemailer=require('nodemailer');
let DIR="./uploads";
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null,DIR)
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now()+ '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
    }
  })
let upload = multer({ storage: storage }).single('attach');

//end
//send mail setting 
const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: 'eshopperonlineshopping@gmail.com', // generated ethereal user
      pass: 'eshopper123' // generated ethereal password
    },
    tls: {
        rejectUnauthorized: false
    }
  });
//end
//connect express to mongodb (mongoose)
const mongoose=require('mongoose');
  mongoose.connect('mongodb://localhost/react1pmproject',
   {useNewUrlParser: true, useUnifiedTopology: true,createIndexes:true},(err)=>
   {
       if(err){}
       else { console.log("Mongoddb connected")}
   });
 
const PORT=8899;
const app=express();
app.use(cors());//allow cross orgin request
app.use(bodyParser.json());//parse body data into json
app.use(express.static('uploads'));
const adminModel=require('./db/adminschema');
const catModel=require('./db/catschema');
const proModel=require('./db/proschema');
//const conModel=require('./db/Contact');
//const userModel=require('./db/userSchema');
//routes | api 
app.post("/register",(req,res)=>
{
     //read email and password from post api 
     let postemail=req.body.email;
     let postpassword=sha1(req.body.password);
    let ins=new adminModel({email:postemail,password:postpassword});
    ins.save((err)=>
    {
        if(err){
            res.json({'err':1,'msg':'user already registerd'})
        }
        else 
        {
            res.json({'err':0,'msg':'User registerd successfully'})
        }
    })

})
app.post("/adminlogin",(req,res)=>
{
    //read email and password from post api 
    let postemail=req.body.email;
    let postpassword=sha1(req.body.password);
    adminModel.findOne({email:postemail,password:postpassword},(err,data)=>
    {
        if(err){ res.json({'err':1,'msg':'Something went wrong'})}
        else if(data==null)
        {
            res.json({'err':1,'msg':'Email or password is not correct'})
        }
        else 
        {
            res.json({'err':0,'msg':'login success','uid':postemail})
        }
    })
})
app.post("/changepassword",(req,res)=>
{
    let op=sha1(req.body.op);
    let np=sha1(req.body.np);
    let uid=req.body.uid;
    adminModel.findOne({email:uid},(err,data)=>
    {
        if(err){ res.json({'err':1,'msg':'Something wrong'})}
        else {
            console.log(data)
            if(op==data.password)
            {
                 if(op==np)
                 {
                    res.json({'err':1,'msg':'Old pass and new pass is not same'})
                 }
                 else 
                 {
                     adminModel.updateOne({email:uid},{$set:{password:np}},(err)=>
                     {
                         if(err){ res.json({'err':1,'msg':'Something wrong'})}
                         else 
                         {
                            res.json({'err':0,'msg':'Password Change Successfully'})              
                         }
                     })
                 }
            }
            else 
            {
                res.json({'err':1,'msg':'Old pass is not correct'})  
            }
        }
    })
})
app.post("/addcategory",(req,res)=>
{
   upload(req,res,err=>
    {
        if(err){
        res.json({'err':1,'msg':'Uploading error'});
        }
        else{
        let cname=req.body.cname;
        let fname=req.file.filename;
     let ins=new catModel({cname:cname,image:fname});
     ins.save(err=>
        {
            if(err){ 
                res.json({'err':1,'msg':'catgory already added'})
            }
            else{
                res.json({'err':0,'msg':'catgory  added'})    
            }
        })
        }
    })
})

app.post("/addproduct",(req,res)=>
{
   upload(req,res,err=>
    {
        if(err){
        res.json({'err':1,'msg':'Uploading error'});
        }
        else{
        	console.log(req.body);
        let cname=req.body.cname;
        let fname=req.file.filename;
        let pname=req.body.pname;
        let price=req.body.price;
        let quantity=req.body.quantity;
     let ins=new proModel({cname:cname,pname:pname,price:price,quantity:quantity,image:fname});
     ins.save(err=>
        {
            if(err){ 
            	console.log(err);
                res.json({'err':1,'msg':'product already added'})
            }
            else{
                res.json({'err':0,'msg':'product added'})    
            }
        })
     	// res.json({'err':0, 'msg': 'Done'});
        }
    })
})

app.get("/getcategory",(req,res)=>
{
   catModel.find({},(err,data)=>
   {
       if(err){}
       else{
           res.json({'err':0,'catdata':data})
       }
   }) 
})
app.get("/delcategory/:id",(req,res)=>
{
    let cid=req.params.id;
    catModel.deleteOne({_id:cid},(err)=>
    {
        if(err){}
        else{
            res.json({'err':0,'msg':'deleted'})
        }
    })
})
app.get("/getproduct/:cn?",(req,res)=>
{
    let cname=req.params.cn;
    if(cname!=undefined)
    {
        proModel.find({cname:cname},(err,data)=>
        {
            if(err){}
            else{
                res.json({'err':0,'prodata':data})
            }
        }) 
    }
    else {
   proModel.find({},(err,data)=>
   {
       if(err){}
       else{
           res.json({'err':0,'prodata':data})
       }
   }) 
 }
})
app.post("/getcartdata",(req,res)=>
{
    let cdata=req.body.cdata;
    proModel.find({_id:{$in:cdata}},(err,data)=>
    {
        if(err){ res.json({'err':1,'msg':'Wrong'})}
        else 
        {
            res.json({'err':0,'cartdata':data})
        }
    })
  
})

app.post("/postcontact",(req,res)=>
{
    let name=req.body.name;
    let email=req.body.email;
    let subject=req.body.subject;
    let message=req.body.message;
    //mail options
    const mailOptions={
        from:'xyz@gmail.com',
        to:['vartikasrivastava999@gmail.com','nitiknarang77@gmail.com','ashishbaranwal361@gmail.com','joshisummi@gmail.com'],
        subject:"Feedback from website",
        html:`Hello <br/> Name : ${name} <br/> Email: ${email} <br/>Subject : ${subject} <br/> Message : ${message}` 
    }
    //send mail 
    transporter.sendMail(mailOptions,(err,data)=>
    {
        if(err){
            res.json({'err':1,'msg':'Contact not send'})
        }
        else 
        {
            let ins=new conModel({name:name,email:email,subject:subject,message:message});
            ins.save(err=>
                {
                if(err)
                {
                    res.json({'err':1,'msg':'Contact not send'})
                }
                else {
                    res.json({'err':0,'msg':'Contact  send'})
                }
            })

        }
    })
})
app.post("/userregister",(req,res)=>
{
//read email and password from post api 
let postemail=req.body.email;
let postpassword=sha1(req.body.password);
let postname=req.body.name
let ins=new userModel({email:postemail,password:postpassword,name:postname});
ins.save((err)=>
{
   if(err){
       res.json({'err':1,'msg':'user already registerd'})
   }
   else 
   {
       res.json({'err':0,'msg':'User registerd successfully'})
   }
})
})
app.post("/userlogin",(req,res)=>
{
    let postemail=req.body.email;
    let postpassword=sha1(req.body.password);
    userModel.findOne({email:postemail,password:postpassword},(err,data)=>
    {
        if(err){ res.json({'err':1,'msg':'Something went wrong'})}
        else if(data==null)
        {
            res.json({'err':1,'msg':'Email or password is not correct'})
        }
        else 
        {
            res.json({'err':0,'msg':'login success','uid':postemail})
        }
    })
})
app.listen(PORT,()=>
{
    console.log(`Work on ${PORT}`);
})








