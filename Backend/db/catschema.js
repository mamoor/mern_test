const mongoose=require('mongoose');
let catSchema=new mongoose.Schema({
    cname:{type:String,unique:true},
    image:{type:String,required:true},
   created_at:{type:Date,default:Date.now}
})
module.exports=mongoose.model('cat',catSchema);