const mongoose=require('mongoose');
let proSchema=new mongoose.Schema({
    pname:{type:String,unique:true},
    cname:{type:String,unique:false},
    price:{type:Number},
    quantity:{type:Number},
    image:{type:String,required:true},
   created_at:{type:Date,default:Date.now}
})
module.exports=mongoose.model('pro',proSchema);