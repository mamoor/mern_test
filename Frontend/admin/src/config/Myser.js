import axios from 'axios';
import {MAIN_URL} from './Url';
export function postAdminLogin(data)
{
    return axios.post(`${MAIN_URL}adminlogin`,data)
}
export function postChangePassword(data)
{
    return axios.post(`${MAIN_URL}changepassword`,data)
}
export function postaddcategory(data)
{
    return axios.post(`${MAIN_URL}addcategory`,data)
}
export function postaddproduct(data)
{
    return axios.post(`${MAIN_URL}addproduct`,data)
}
export function getcat()
{
    return axios.get(`${MAIN_URL}getcategory`);
}
export function delcat(id)
{
    return axios.get(`${MAIN_URL}delcategory/${id}`);
}
export function register(data) {
    return axios.post(`${MAIN_URL}register`,data);
}