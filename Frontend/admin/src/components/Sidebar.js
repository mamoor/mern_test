import React, { Component } from 'react'
import {Link} from 'react-router-dom';
export class Sidebar extends Component {
    render() {
        return (
            <div class="list-group">
  <Link to="/dashboard/category" class="list-group-item list-group-item-action active">
    Category
  </Link>
  <Link to="/dashboard/products" class="list-group-item list-group-item-action">Products</Link>
  <Link to="/dashboard/orders" class="list-group-item list-group-item-action">Orders</Link>
  <Link to="/dashboard/feedback" class="list-group-item list-group-item-action">Feedback</Link>
  
</div>
        )
    }
}

export default Sidebar
