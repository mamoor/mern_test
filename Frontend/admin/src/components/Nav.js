import React, { Component } from 'react'
import {Link} from 'react-router-dom';
export class Nav extends Component {
    constructor(props)
    {
        super(props);
        this.state={uid:localStorage.getItem('uid')}
    }
    componentDidMount()
    {
        if(localStorage.getItem('uid')==undefined)
        {
            this.props.history.push('/');
        }
    }
    signOut=()=>
    {
        if(window.confirm("Do u want to logout ?"))
        {
            localStorage.removeItem('uid');
            this.props.history.push('/');
        }
    }
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
  <Link className="navbar-brand" to="/dashboard">Dashboard</Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <Link className="nav-link" to="/dashboard">Home <span className="sr-only">(current)</span></Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="/dashboard/cpass">Change Password</Link>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Welcome : {this.state.uid}</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="javascript:void(0)" onClick={this.signOut}>Logout</a>
      </li>
      </ul>
    
  </div>
</nav>
        )
    }
}

export default Nav
