import React, { Component } from 'react'
import {postAdminLogin, register} from '../config/Myser';
export class Login extends Component {
    constructor(props)
    {
        super(props);
        this.state={email:'',password:'',check:'',errMsg:''}
    }
    handler=(event)=>
    {
        const {name,value}=event.target;
        this.setState({[name]:value})
    }
    postLogin=(event)=>
    {
       event.preventDefault();
       postAdminLogin(this.state)
       .then(res=>
        {
            if(res.data.err==0)
            {
              //then store uid in localstorage 
            localStorage.setItem('uid',res.data.uid);
            //redirect 
            this.props.history.push('/dashboard');
            }
            if(res.data.err==1)
            {
                this.setState({errMsg:res.data.msg})
            }
        })
    }
    render() {
        return (
            <div>
                <main>
                    <header className="jumbotron">
                <h1 className="text-center">Admin Panel</h1>
                    </header>
                    <section className="container">
            {this.state.errMsg!=''&& 
            <div className="alert alert-danger">{this.state.errMsg}</div>}
                <form onSubmit={this.postLogin}>
                   <div className="form-group">
                    <label>Email</label>
                    <input type="text" name="email" className="form-control" onChange={this.handler}/>
                   </div>
                   <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" onChange={this.handler}/>
                   </div>
                   <div className="form-group">
                    
                    <input type="checkbox" name="check"  onChange={this.handler}/>
                    <label>Remember Me</label>
                   </div>
                   <input type="submit" value="Login" className="btn btn-success"/>
                </form>
                    </section>
                    
                </main>
            </div>
        )
    }
}

export default Login
