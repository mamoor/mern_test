import React, { Component } from 'react'
import Nav from './Nav'
import Sidebar from './Sidebar'
import {Link} from 'react-router-dom';
import {getcat,delcat} from '../config/Myser';
import {MAIN_URL} from '../config/Url';
export class Category extends Component {
    constructor(props)
    {
        super(props);
        this.state={catData:[]}
    }
    componentDidMount()
    {
        getcat()
        .then(res=>
            {
              if(res.data.err==0)
              {
                  this.setState({catData:res.data.catdata})
              }
            })
    }
    delCat=(id)=>
    {
        if(window.confirm("Do u want to delete"))
        {
           delcat(id)
           .then(res=>
            {
                if(res.data.err==0){
                    alert(res.data.msg)
                }
                if(res.data.err==1){
                    alert(res.data.msg)
                }
                
            })
        }
        
    }
    render() {
        return (
            <main>
                <header>
                   <Nav {...this.props}/>
                </header>
                <br/>
                <section className="row container">
                    <aside className="col-sm-4">
                        <Sidebar/>
                    </aside>
                    <aside className="col-sm-8">
                        <h3>Category</h3>
            <table className="table">
                <tr>
                    <th colspan={5} className="text-center">
                        <Link to="/dashboard/addcat" className="btn btn-danger">Add category</Link>
                    </th>
                </tr>
                <tr>
                    <th>S.no</th>
                    <th>Cname</th>
                    <th>Image</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            {this.state.catData.map((cat,ind)=>
            <tr>
                <td>{ind+1}</td>
                <td>{cat.cname}</td>
                <td><img src={`${MAIN_URL}${cat.image}`} width={50} height={50}/></td>
                <td>{cat.created_at}</td>
                <td>Edit 
                    <button onClick={()=>this.delCat(cat._id)}>Delete</button></td>
            </tr>
            )}
            </table>
                    </aside>
                </section>
            </main>
        )
    }
}

export default Category
