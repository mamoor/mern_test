import React, { Component } from 'react'
import Nav from './Nav'
import Sidebar from './Sidebar'
import {postChangePassword} from '../config/Myser';
export class Changepass extends Component {
    constructor(props)
    {
        super(props);
        this.state={op:'',np:'',cp:'',errMsg:'',succMsg:''}
    }
    handler=(event)=>
    {
        const {name,value}=event.target;
        this.setState({[name]:value});
    }
    changePassword=(e)=>
    {
     e.preventDefault();
      if(this.state.np==this.state.cp)
      {
          let formData={op:this.state.op,np:this.state.np,uid:localStorage.getItem('uid')};
          postChangePassword(formData)
          .then(res=>
            {
                if(res.data.err==0)
                {
                    this.setState({succMsg:res.data.msg,op:'',np:'',cp:'',errMsg:''})
                }
                if(res.data.err==1)
                {
                    this.setState({errMsg:res.data.msg,op:'',np:'',cp:'',succMsg:''})
                }
            })
      }
      else 
      {
          this.setState({errMsg:'Np and cp is not match',op:'',np:'',cp:'',succMsg:''});
      }
    }
    render() {
        return (
            <main>
                <header>
                   <Nav {...this.props}/>
                </header>
                <br/>
                <section className="row container">
                    <aside className="col-sm-4">
                        <Sidebar/>
                    </aside>
                    <aside className="col-sm-8">
                        <h3>Change Password</h3>
            {this.state.succMsg!=''&& 
            <div className="alert alert-success">{this.state.succMsg}</div>}
                        {this.state.errMsg!=''&& 
            <div className="alert alert-danger">{this.state.errMsg}</div>}
            <form onSubmit={this.changePassword}>
              <div className="form-group">
                  <label>Old Pass</label>
                  <input type="password" name="op" className="form-control" onChange={this.handler}/>
              </div>
              <div className="form-group">
                  <label>New Pass</label>
                  <input type="password" name="np" className="form-control" onChange={this.handler}/>
              </div>
              <div className="form-group">
                  <label>Con Pass</label>
                  <input type="password" name="cp" className="form-control" onChange={this.handler}/>
              </div>
              <input type="submit" value="Submit" className="btn btn-success"/>
            </form>
                    </aside>
                </section>
            </main>
        )
    }
}

export default Changepass
