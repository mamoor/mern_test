import React, { Component } from 'react'
import Nav from './Nav'
import Sidebar from './Sidebar'
import {Link} from 'react-router-dom';
import {postaddcategory,getcat,postaddproduct} from '../config/Myser';
export class AddProduct extends Component {
    constructor(props)
    {
        super(props);
        this.state={cname:'',imagePath:'',errMsg:'',catData:[],pname:'',price:null,quantity:null}
    }
    componentDidMount()
    {
        getcat()
        .then(res=>{
            if(res.data.err==0)
            {
                this.setState({catData:res.data.catdata})
            }
        })

    }
    handler=(event)=>{
        this.setState({[event.target.name]:event.target.value})
    }
    imageUpload=(e)=>
    {
       if(e.target.files.length>0)
       {
           console.log(e.target.files[0])
           this.setState({imagePath:e.target.files[0]})
       } 
    }
    addpro=(e)=>
    {
        e.preventDefault();
        if(this.state.imagePath!='')
        {
            if(this.state.imagePath.type=="image/jpg" || this.state.imagePath.type=="image/png")
            {
        //when we send data with attahmnt we use Formdata proprty
        let formdata=new FormData();
        formdata.append('cname',this.state.cname);
        formdata.append('pname',this.state.pname);
        formdata.append('price',this.state.price);
        formdata.append('quantity',this.state.quantity);
    formdata.append('attach',this.state.imagePath);
    postaddproduct(formdata)
    .then(res=>
        {
            if(res.data.err==0)
            {
                alert(res.data.msg);
                this.props.history.push('/dashboard/category')
            }
            if(res.data.err==1)
            {
                alert(res.data.msg)
            }
        })
            }
            else{
                this.setState({errMsg:'Only jpg or png supported'})
            }
        }
        else{
    this.setState({errMsg:'Plz selet a image'})
        }
    }
    render() {
        return (
            <main>
                <header>
                   <Nav {...this.props}/>
                </header>
                <br/>
                <section className="row container">
                    <aside className="col-sm-4">
                        <Sidebar/>
                    </aside>
                    <aside className="col-sm-8">
                        <h3>Add Product</h3>
        {this.state.errMsg!='' && 
        <div className="alert alert-danger">
            {this.state.errMsg}</div>}
            <form onSubmit={this.addpro}>
                <div className="form-group">
                    <label>Cname</label>
                        <select name="cname" className="form-control" onChange={this.handler}>
                            <option value="">Select One</option>
                            {this.state.catData.map(cat=>
                                <option value={cat.cname}>{cat.cname}</option>
                                )}
                        </select>
                </div>
                <div className="form-group">
                    <label>Pname</label>
                    <input type="text" name="pname" className="form-control" onChange={this.handler}/>
                </div>
                <div className="form-group">
                    <label>Price</label>
                    <input type="text" name="price" className="form-control" onChange={this.handler}/>
                </div>
                <div className="form-group">
                    <label>Quantity</label>
                    <input type="text" name="quantity" className="form-control" onChange={this.handler}/>
                </div>
                <div className="form-group">
                    <label>Image</label>
                    <input type="file" name="image" className="form-control" onChange={this.imageUpload}/>
                </div>
                <input type="submit" value="Submit" className="btn btn-success"/>
            </form>
                    </aside>
                </section>
            </main>
        )
    }
}

export default AddProduct
