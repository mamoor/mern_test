import React, { Component } from 'react'
import Nav from './Nav'
import Sidebar from './Sidebar'

export class Dashboard extends Component {
    render() {
        return (
            <main>
                <header>
                   <Nav {...this.props}/>
                </header>
                <br/>
                <section className="row container">
                    <aside className="col-sm-4">
                        <Sidebar/>
                    </aside>
                    <aside className="col-sm-8">
                        <h3>Welcome to Dashboard</h3>
                    </aside>
                </section>
            </main>
        )
    }
}

export default Dashboard
