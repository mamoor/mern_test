import React, { Component } from 'react'
import Nav from './Nav'
import Sidebar from './Sidebar'
import {Link} from 'react-router-dom';
export class Products extends Component {
    render() {
        return (
            <main>
                <header>
                   <Nav {...this.props}/>
                </header>
                <br/>
                <section className="row container">
                    <aside className="col-sm-4">
                        <Sidebar/>
                    </aside>
                    <aside className="col-sm-8">
                        <h3>Products</h3>
                        <table className="table">
                            <thead>
                                <tr>
                                    <td colSpan={6} className="text-center">
                                        <Link to="/dashboard/addpro" className="btn btn-danger">Add Product</Link>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </aside>
                </section>
            </main>
        )
    }
}

export default Products
