import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import Changepass from './components/Changepass';
import Category from './components/Category';
import Products from './components/Products';
import Orders from './components/Orders';
import Feedback from './components/Feedback';
import AddCategory from './components/AddCategory';
import AddProduct from './components/AddProduct';
function App() {
  return (
     <main>
        <Router>
            <Switch>
                 <Route path="/" exact component={Login}/>
                 <Route path="/dashboard" exact component={Dashboard}/>
                 <Route path="/dashboard/cpass" exact component={Changepass}/>
                 <Route path="/dashboard/category" exact component={Category}/>
                 <Route path="/dashboard/addcat" exact component={AddCategory}/>
                 <Route path="/dashboard/products" exact component={Products}/>
                 <Route path="/dashboard/addpro" exact component={AddProduct}/>
                 <Route path="/dashboard/orders" exact component={Orders}/>
                 <Route path="/dashboard/feedback" exact component={Feedback}/>
            </Switch>
        </Router>
     </main>
  );
}

export default App;
