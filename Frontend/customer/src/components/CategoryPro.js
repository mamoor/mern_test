import React, { Component } from 'react'
import Sidebar from './Sidebar';
import Slider from './Slider';
import {getProByCat} from '../config/Myser'
export class CategoryPro extends Component {
	constructor(props)
	{
		super(props);
		this.state={proData:[],cname:this.props.match.params.cname}
	}
  componentDidMount()
  {
	  getProByCat(this.state.cname)
	  .then(res=>
		{
			if(res.data.err==0)
			{
				this.setState({proData:res.data.prodata})
			}
		})
  }
    render() {
        return (
            <div>
                
	<section>
		<div className="container">
			<div className="row">
				<Sidebar />
				<div className="col-sm-9 padding-right">
					<div className="features_items">
        <h2 className="title text-center">{this.state.cname} Products</h2>
				{this.state.proData.map(pro=>
						<div className="col-sm-4">
							<div className="product-image-wrapper">
								<div className="single-products">
										<div className="productinfo text-center">
											<img src="/images/home/product1.jpg" alt="" />
											<h2>Rs.{pro.price}</h2>
				<p>{pro.pname}</p>
											<a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
										</div>
								</div>
						</div>
						
						)}
				</div>
					
					
					
				</div>
			</div>
		</div>
	</section>
	
            </div>
        )
    }
}

export default CategoryPro
