import React, { Component } from 'react'
import {postContact} from '../config/Myser'
export class Contact extends Component {
	constructor(props){
		super(props);
		this.state={name:'',email:'',subject:'',message:''}
	}
	handler=(e)=>
	{
		const {name,value}=e.target;
		this.setState({[name]:value})
	}
	sendContact=(e)=>
	{
	  e.preventDefault();
	  console.log(this.state)
	  postContact(this.state)
	  .then(res=>
	  {
		  if(res.data.err==0)
		  {
			  alert(res.data.msg)
		  }
		  if(res.data.err==1)
		  {
			  alert(res.data.msg)
		  }
	  })
	}
    render() {
        return (
            <div>
                <div id="contact-page" className="container">
    	<div className="bg">
	    	<div className="row">    		
	    		<div className="col-sm-12">    			   			
					<h2 className="title text-center">Contact <strong>Us</strong></h2>    			    				    				
					
				</div>			 		
			</div>    	
    		<div className="row">  	
	    		<div className="col-sm-8">
	    			<div className="contact-form">
	    				<h2 className="title text-center">Get In Touch</h2>
	    				<div className="status alert alert-success"></div>
				    	<form onSubmit={this.sendContact} id="main-contact-form" className="contact-form row" name="contact-form" method="post">
				            <div className="form-group col-md-6">
				                <input type="text" name="name" className="form-control" required="required" placeholder="Name" onChange={this.handler}/>
				            </div>
				            <div className="form-group col-md-6">
				                <input type="email" name="email" className="form-control" required="required" placeholder="Email"  onChange={this.handler}/>
				            </div>
				            <div className="form-group col-md-12">
				                <input type="text" name="subject" className="form-control" required="required" placeholder="Subject"  onChange={this.handler}/>
				            </div>
				            <div className="form-group col-md-12">
				                <textarea name="message" id="message" required="required" className="form-control" rows="8" placeholder="Your Message Here"  onChange={this.handler}></textarea>
				            </div>                        
				            <div className="form-group col-md-12">
				                <input type="submit" name="submit" className="btn btn-primary pull-right" value="Submit"/>
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div className="col-sm-4">
	    			<div className="contact-info">
	    				<h2 className="title text-center">Contact Info</h2>
	    				<address>
	    					<p>E-Shopper Inc.</p>
							<p>935 W. Webster Ave New Streets Chicago, IL 60614, NY</p>
							<p>Newyork USA</p>
							<p>Mobile: +2346 17 38 93</p>
							<p>Fax: 1-714-252-0026</p>
							<p>Email: info@e-shopper.com</p>
	    				</address>
	    				<div className="social-networks">
	    					<h2 className="title text-center">Social Networking</h2>
							<ul>
								<li>
									<a href="#"><i className="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i className="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i className="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="#"><i className="fa fa-youtube"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div>
            </div>
        )
    }
}

export default Contact
