import React, { Component } from 'react'
import {getCartData} from '../config/Myser';
export class Cart extends Component {
    constructor(props){
        super(props);
        this.state={cartData:[]}
    }
   componentDidMount()
   {
       if(localStorage.getItem('acart')!=undefined)
       {
		   let arr=JSON.parse(localStorage.getItem('acart'));
		   getCartData({cdata:arr})
		   .then(res=>
			{
				if(res.data.err==0)
				{
					this.setState({cartData:res.data.cartdata})
				}
			})
       }
   }
    render() {
        return (
            <div>
                 <section id="cart_items">
		<div className="container">
			<div className="breadcrumbs">
				<ol className="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li className="active">Shopping Cart</li>
				</ol>
			</div>
			<div className="table-responsive cart_info">
				<table className="table table-condensed">
					<thead>
						<tr className="cart_menu">
							<td className="image">Item</td>
							<td className="description"></td>
							<td className="price">Price</td>
							<td className="quantity">Quantity</td>
							<td className="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
				{this.state.cartData.map(cart=>
						<tr>
							<td className="cart_product">
								<a href=""><img src="images/cart/one.png" alt=""/></a>
							</td>
							<td className="cart_description">
								<h4><a href="">{cart.pname}</a></h4>
								<p>Category: {cart.cname}</p>
							</td>
							<td className="cart_price">
				                 <p>Rs.{cart.price}</p>
							</td>
							<td className="cart_quantity">
								<div className="cart_quantity_button">
									<a className="cart_quantity_up" href=""> + </a>
									<input className="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2"/>
									<a className="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td className="cart_total">
								<p className="cart_total_price">$59</p>
							</td>
							<td className="cart_delete">
								<a className="cart_quantity_delete" href=""><i className="fa fa-times"></i></a>
							</td>
						</tr>

						
)}					
</tbody>
				</table>
			</div>
		</div>
	</section>
             </div>
        )
    }
}

export default Cart 
