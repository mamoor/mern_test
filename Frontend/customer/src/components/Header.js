import React, { Component } from 'react'
import {getCat} from '../config/Myser';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux'
export class Header extends Component {
	constructor(props)
	{
		super(props);
		this.state={catData:[],count:0}
	}
  componentDidMount()
  {
	  if(localStorage.getItem('acart')!=undefined)
	  {
		  let arr=JSON.parse(localStorage.getItem('acart'));
		  this.setState({count:arr.length})
	  }
	  getCat()
	  .then(res=>
		{
			if(res.data.err==0)
			{
				this.setState({catData:res.data.catdata})
			}
		})
  }
    render() {
        return (
            <header id="header">
		<div className="header_top">
			<div className="container">
				<div className="row">
					<div className="col-sm-6">
						<div className="contactinfo">
							<ul className="nav nav-pills">
								<li><a href="#"><i className="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i className="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="social-icons pull-right">
							<ul className="nav navbar-nav">
								<li><a href="#"><i className="fa fa-facebook"></i></a></li>
								<li><a href="#"><i className="fa fa-twitter"></i></a></li>
								<li><a href="#"><i className="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i className="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div className="header-middle">
			<div className="container">
				<div className="row">
					<div className="col-sm-4">
						<div className="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div>
						<div className="btn-group pull-right">
							<div className="btn-group">
								<button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span className="caret"></span>
								</button>
								<ul className="dropdown-menu">
									<li><a href="#">Canada</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div className="btn-group">
								<button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span className="caret"></span>
								</button>
								<ul className="dropdown-menu">
									<li><a href="#">Canadian Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div className="col-sm-8">
						<div className="shop-menu pull-right">
							<ul className="nav navbar-nav">
								<li><a href="#"><i className="fa fa-user"></i> Account</a></li>
								<li><a href="#"><i className="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i className="fa fa-crosshairs"></i> Checkout</a></li>
								<li><Link to="/cart"><i className="fa fa-shopping-cart"></i> Cart
								{this.props.mycart.length>0?
								<span>({this.props.mycart.length})</span>:
								<label>
									{this.state.count>0?
									<span>({this.state.count})</span>:''}</label>}
								</Link></li>
								<li><Link to="/login"><i className="fa fa-lock"></i> Login</Link></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div className="header-bottom">
			<div className="container">
				<div className="row">
					<div className="col-sm-9">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
						</div>
						<div className="mainmenu pull-left">
							<ul className="nav navbar-nav collapse navbar-collapse">
								<li><Link to="/" className="active">Home</Link></li>
								<li className="dropdown"><a href="#">Category<i className="fa fa-angle-down"></i></a>
                                    <ul role="menu" className="sub-menu">
									{this.state.catData.map(cat=>
                                        <li><Link to={`/category/${cat.cname}`}>{cat.cname}</Link></li>
										)}
                                    </ul>
                                </li> 
								
								<li><Link to="/contact">Contact</Link></li>
							</ul>
						</div>
					</div>
					<div className="col-sm-3">
						<div className="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
        )
    }
}
const mapStateToProps=(state)=>
{
	return {
		mycart:state.pcart
	}
}
export default connect(mapStateToProps)(Header)
