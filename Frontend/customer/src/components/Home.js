import React, { Component } from 'react'
import Sidebar from './Sidebar';
import Slider from './Slider';
import {getPro} from '../config/Myser';
import {MAIN_URL} from '../config/Url';
import {connect} from 'react-redux'
export class Home extends Component {
	constructor(props)
	{
		super(props);
		this.state={proData:[]}
	}
  componentDidMount()
  {
	  getPro()
	  .then(res=>
		{
			if(res.data.err==0)
			{
				this.setState({proData:res.data.prodata})
			}
		})
  }
  addCart=(pid)=>
  {
	if(localStorage.getItem('acart')!=undefined){
		let arr=JSON.parse(localStorage.getItem('acart'));
		if(arr.includes(pid))
		{
			alert("Product Already added");
		}
		else {
		arr.push(pid);
		localStorage.setItem('acart',JSON.stringify(arr));
		this.props.dispatchcart(arr)
		alert("Product Add to Cart");
		}
	}  
	else 
	{
		let arr=[];
		arr.push(pid);
		localStorage.setItem('acart',JSON.stringify(arr));
		this.props.dispatchcart(arr);
		alert("Product Add to Cart");
	}
  }
    render() {
        return (
            <div>
                <Slider />
	<section>
		<div className="container">
			<div className="row">
				
				<div className="col-sm-9 padding-right">
					<div className="features_items">
						<h2 className="title text-center">Features Items</h2>
				{this.state.proData.map(pro=>
						<div className="col-sm-4">
							<div className="product-image-wrapper">
								<div className="single-products">
										<div className="productinfo text-center">
											<img src={`${MAIN_URL}${pro.image}`} style={{height:"250px"}} alt="" />
											<h2>Rs.{pro.price}</h2>
				<p>{pro.pname}</p>
											<a href="javascript:void(0)" onClick={()=> this.addCart(pro._id)} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
										</div>
								</div>
						</div>
						
						)}
				</div>
					
					
					
				</div>
			</div>
		</div>
	</section>
	
            </div>
        )
    }
}
const mapDispatchToProps=(dispatch)=>
{
	return {
		dispatchcart:(data)=>
		{
			dispatch({type:'addcart',payload:data})
		}
	}
}
export default connect(null,mapDispatchToProps)(Home)
