import React from 'react';
import {BrowserRouter as Router,Route ,Switch} from 'react-router-dom'
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './components/Home';
import Contact from './components/Contact';
import CategoryPro from './components/CategoryPro';
import Cart from './components/Cart';
import Login from './components/Login';

function App() {
  return (
    <div>
      <Router>
        <Header />
        <section>
           <Switch>
           <Route path="/" exact component={Home}/>
           <Route path="/contact" exact component={Contact}/>
           <Route path="/cart" exact component={Cart}/>
           <Route path="/login" exact component={Login}/>
           <Route path="/category/:cname" exact component={CategoryPro}/>
           </Switch>
        </section>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
