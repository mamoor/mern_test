import axios from 'axios';
import {MAIN_URL} from './Url';
export function getCat()
{
    return axios.get(`${MAIN_URL}getcategory`)
}
export function getPro()
{
    return axios.get(`${MAIN_URL}getproduct`)
}
export function getProByCat(cname)
{
    return axios.get(`${MAIN_URL}getproduct/${cname}`)
}
export function getCartData(data)
{
    return axios.post(`${MAIN_URL}getcartdata`,data)
}
export function postContact(data)
{
    return axios.post(`${MAIN_URL}postcontact`,data)
}